package jpacman.view;

import jpacman.view.graphics.Screen;

public interface Renderer
{
    abstract public void render(Screen screen);
}
